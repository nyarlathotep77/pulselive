# README #

RedMart assignment

### How do I get set up? ###

* The app was developed on Android Studio 3.0.
* The app has been tested down to Android API 19

### Architecture ###
* The app uses a MVVM architecture implemented via the Android Architecture components.

### UI ###
#### SplashScreenActivity ####
com.damageinc.redmart.ui.splash.SplashScreenActivity is self-explanatory. It just presents a branded screen when the app starts

#### ProductListActivity ####
It presents a paged list of products. This Activity doesn't use any fragment. There's no particular reason for that apart from showing that it's legitimate to do so.
com.damageinc.redmart.domain.viewmodels.ProductListViewModel is responsible for asynchronously fetching the paged product list and for notifying ProductListActivity when new data are available via LiveData.

#### ProductDetailsActivity ####
Shows details about the product selected from ProductListActivity. In this case we have a ProductDetailsFragment which thinks about rendering the product details which are added programmatically into vertical LinearLayout.
Also in this case we make use of a ViewModel, ProductDetailsViewModel, to run the relevant asynchronous network IO and for notying ProductDetailsFragment via LiveData.

### Other info ###

* In order to implement the list-detail UI, I've just used the related template from Android Studio.
* The app was developed in Kotlin
* The app uses part of the new Android Architecture components, in particular it makes use of ViewModel.

### Used libraries ###

* Retrofit for implementing the REST layer
* GreenRobot EventBus to establish a communication channel between network Jobs and ViewModels
* JobManager to execute the REST operations asynchronously. Retrofit is able to run REST operations asynchronously, but in real life we may often deal with additional logic around networking, i.e. caching or executing operantions in a DB relevant to a particular REST transaction.
* Glide to load images.
* Dagger for Dependency Injection
* Mockito for creating test mocks.

### Tests ###

I provided an example of a Unit Test in ProductListViewModelTest where I also mock some of the dependencies provided via Dagger.
There's also an Espresso test which I've recorded via Android Studio, which in this way is able to create instrumentation tests by just running the app.

### Animations ###
I've provided a simple Shared Element animation when pressing the back button from the product detail to the list screen. The animation is on the image.

### Custom Views ###
I've created a simple extension to CardView called DetailCardView to render the individual product details.

### Redmart UI/UX style ###
RedMart splashscreen on app start and RedMart theme created with the colour scheme of the RedMart shopping app.

