package com.damageinc.redmart.infrastructure.di.module

import android.app.Application
import android.content.Context
import com.birbit.android.jobqueue.JobManager
import com.damageinc.redmart.infrastructure.di.modules.ApplicationModule
import org.greenrobot.eventbus.EventBus
import org.mockito.Mockito

class TestApplicationModule : ApplicationModule() {

    override fun provideApplicationContext(): Context {
        return Mockito.mock(Application::class.java)
    }

    override fun provideEventBus(): EventBus {
        return Mockito.mock(EventBus::class.java)
    }

    override fun provideJobManager(context: Context): JobManager {
        return Mockito.mock(JobManager::class.java)
    }
}