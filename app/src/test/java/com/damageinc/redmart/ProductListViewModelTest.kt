package com.damageinc.redmart

import com.damageinc.redmart.domain.models.Product
import com.damageinc.redmart.domain.models.network.search.SearchResponse
import com.damageinc.redmart.domain.viewmodels.ProductListViewModel
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response
import android.arch.core.executor.testing.InstantTaskExecutorRule;
import com.damageinc.redmart.infrastructure.di.Injector
import com.damageinc.redmart.infrastructure.di.components.ApplicationComponent
import com.damageinc.redmart.infrastructure.di.components.DaggerApplicationComponent
import com.damageinc.redmart.infrastructure.di.module.TestApplicationModule
import org.junit.*


class ProductListViewModelTest {
    @get:Rule
    public val instantExecutorRule = InstantTaskExecutorRule()
    var productListViewModel: ProductListViewModel? = null
    val firstPage = mutableListOf<Product>().apply {
        val product1 = Product(23, "Banana")
        val product2 = Product(392784, "Cherries")
        add(product1)
        add(product2)
    }

    val secondPage = mutableListOf<Product>().apply {
        val product1 = Product(876, "Eggs")
        val product2 = Product(96987, "Whole chicken")
        add(product1)
        add(product2)
    }

    @Before
    fun setup() {
        Injector.instance.applicationComponent = DaggerApplicationComponent.builder().applicationModule(TestApplicationModule()).build()
        productListViewModel = ProductListViewModel()
    }

    @After
    fun tearDown() {
        Injector.instance.applicationComponent = null
        productListViewModel = null
    }

    @Test
    fun productPagesShouldBeAppendedCorrectly() {
        productListViewModel?.products?.value = firstPage

        val searchResponse = SearchResponse(products = secondPage, page = 2, pageSize = 2)
        productListViewModel?.onSuccess(searchResponse)
        Assert.assertEquals(4, productListViewModel?.products?.value?.size)
        Assert.assertEquals(productListViewModel?.products?.value?.last()?.title, "Whole chicken")
    }

}