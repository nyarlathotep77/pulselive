package com.damageinc.redmart.ui.productdetails

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.graphics.Palette
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.bumptech.glide.request.transition.Transition
import com.damageinc.redmart.R
import com.damageinc.redmart.common.OnResultListener
import com.damageinc.redmart.infrastructure.network.retrofit.RedMartRestApi
import com.damageinc.redmart.ui.common.BaseActivity
import com.damageinc.redmart.ui.productlist.ProductListActivity
import kotlinx.android.synthetic.main.activity_item_detail.*


/**
 * An activity representing a single Item detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [ProductListActivity].
 */
class ProductDetailsActivity : BaseActivity() {

    companion object {
        val TOOLBAR_IMAGE_URL_ARG = "toolbar_image_url"

        fun start(parent: Activity, args: Bundle?, sourceView: View) {
            val intent = Intent(parent, ProductDetailsActivity::class.java)
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(parent, sourceView, "profile")
            args?.let { intent.putExtras(args) }
            startActivity(parent, intent, options.toBundle())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            val fragment = ProductDetailsFragment.newInstance(intent.extras)
            supportFragmentManager.beginTransaction()
                    .add(R.id.productDetailContainer, fragment)
                    .commit()
        }
    }

    override fun onResult(senderTag: String?, result: OnResultListener.Result, args: Bundle?) {
        super.onResult(senderTag, result, args)
        val imageUrl = args?.getString(TOOLBAR_IMAGE_URL_ARG)
        collapsingToolbar.title = args?.getString(TOOLBAR_TITLE_ARG, "");
        imageUrl?.let {
            Glide.with(this)
                    .asBitmap()
                    .load("${RedMartRestApi.IMAGE_BASE_URL}${imageUrl}")
                    .apply(RequestOptions().centerCrop())
                    .into(object : BitmapImageViewTarget(image) {
                        override fun onResourceReady(resource: Bitmap?, transition: Transition<in Bitmap>?) {
                            super.onResourceReady(resource, transition)
                            val palette = Palette.from(resource).generate()
                            setToolbarColor(palette)
                        }
                    })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    navigateUpTo(Intent(this, ProductListActivity::class.java))
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }


    private fun setToolbarColor(palette: Palette) {
        val colour = palette.lightVibrantSwatch?.titleTextColor ?: ContextCompat.getColor(this, (R.color.colorAccent))
        toolbar.setTitleTextColor(colour)
        collapsingToolbar.setExpandedTitleColor(colour)
        val drawable = toolbar.navigationIcon
        drawable?.setColorFilter(colour, PorterDuff.Mode.SRC_ATOP)
    }
}
