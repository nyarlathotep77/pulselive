package com.damageinc.redmart.ui.productlist

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.damageinc.redmart.R
import com.damageinc.redmart.domain.models.Pricing
import com.damageinc.redmart.domain.models.Product
import com.damageinc.redmart.infrastructure.network.retrofit.RedMartRestApi
import kotlinx.android.synthetic.main.product_list_content.view.*

class ProductListAdapter(private val onClickListener: View.OnClickListener,
                         var products: MutableList<Product>?) :
        RecyclerView.Adapter<ProductListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.product_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = products?.get(position)
        Log.d("CazziAmari", "Binding position ${position} for product ${product?.title}")
        holder.title.text = product?.title
        holder.price.text = Pricing.getFormattedPrice(product?.pricing?.price)
        Glide.with(holder.productImage.context)
                .asBitmap()
                .load("${RedMartRestApi.IMAGE_BASE_URL}${product?.image?.name}")
                .apply(RequestOptions()
                        .centerCrop())
                .into(holder.productImage)

        with(holder.itemView) {
            tag = product
            setOnClickListener(onClickListener)
        }
    }


    override fun getItemCount(): Int {
        return products?.size ?: 0
    }

    fun addProducts(products: List<Product>) {
        Log.d("CazziAmari", "Trying to add ${products.size} products into the list")
        val oldSize = this.products?.size
        this.products?.addAll(products.subList(oldSize ?: 0, products.size))
        oldSize?.let {
            notifyItemInserted(oldSize)
            Log.d("CazziAmari", "more products have been added at position ${oldSize}. Now there are ${this.products?.size} products in the list")
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.title
        val price: TextView = view.price
        val productImage: ImageView = view.productImage
    }
}