package com.damageinc.redmart.ui

import android.content.Context
import android.support.annotation.AttrRes
import android.support.annotation.StyleRes
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.View
import com.damageinc.redmart.R
import kotlinx.android.synthetic.main.card_detail.view.*


class DetailCardView(context : Context, attributeSet: AttributeSet? = null, @AttrRes defStyleAttr : Int) : CardView(context, attributeSet, defStyleAttr) {

    var title : String? = null
    set(value) {
        titleView?.text = value
    }
    var body : String? = null
        set(value) {
            bodyView?.text = value
        }

    init {
        View.inflate(getContext(), R.layout.card_detail, this)
        val attributes = context.theme.obtainStyledAttributes(attributeSet, R.styleable.DetailCardView, 0, 0)

        try {
            title = attributes.getString(R.styleable.DetailCardView_title)
            body = attributes.getString(R.styleable.DetailCardView_body)
        } finally {
            attributes.recycle()
        }
    }
}