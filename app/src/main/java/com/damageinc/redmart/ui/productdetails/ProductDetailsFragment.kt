package com.damageinc.redmart.ui.productdetails

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.damageinc.redmart.R
import com.damageinc.redmart.common.OnResultListener
import com.damageinc.redmart.domain.models.Attribute
import com.damageinc.redmart.domain.models.Product
import com.damageinc.redmart.domain.viewmodels.ProductDetailsViewModel
import com.damageinc.redmart.ui.DetailCardView
import com.damageinc.redmart.ui.common.BaseActivity
import kotlinx.android.synthetic.main.fragment_product_detail.*


/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a [ProductListActivity]
 * in two-pane mode (on tablets) or a [ProductDetailsActivity]
 * on handsets.
 */
class ProductDetailsFragment : Fragment() {

    private var product: Product? = null
    private var listener: OnResultListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_product_detail, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel = ViewModelProviders.of(this).get(ProductDetailsViewModel::class.java)
        if (savedInstanceState == null) {
            viewModel.getProductDetails(arguments.getInt(ARG_PRODUCT_ID))
        } else {
            viewModel.product.value?.let { initView(it) }
        }
        viewModel.product.observe(this, Observer<Product> { productDetails ->
            productDetails?.let { initView(it) }
        })
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as OnResultListener
    }

    private fun initView(product: Product) {
        val toolbarBundle = Bundle()
        toolbarBundle.putString(BaseActivity.TOOLBAR_TITLE_ARG, product.title)
        toolbarBundle.putString(BaseActivity.TOOLBAR_SUBTITLE_ARG, "")
        toolbarBundle.putString(ProductDetailsActivity.TOOLBAR_IMAGE_URL_ARG, product.image?.name)
        addDescriptionCards(product.descriptionFields?.primary)
        addDescriptionCards(product.descriptionFields?.secondary)
        listener?.onResult(TAG, OnResultListener.Result.OK, toolbarBundle)
    }

    private fun addDescriptionCards(attributes : List<Attribute>?) {
        attributes?.let {
            for (attribute in attributes) {
                val detailCardView = DetailCardView(activity, null, R.attr.ProductCardViewStyle)
                // Set the CardView layoutParams
                val params = LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                )
                val baseMargin = resources.getDimensionPixelSize(R.dimen.base_margin)
                params.setMargins(baseMargin, baseMargin, baseMargin, baseMargin);
                detailCardView.layoutParams = params
                detailCardView.title = attribute.name
                detailCardView.body = attribute.content
                descriptionFieldsContainer.addView(detailCardView)
            }
        }
    }

    companion object {
        val TAG = ProductDetailsFragment::class.java.name
        const val ARG_PRODUCT_ID = "productId"

        fun newInstance(arguments: Bundle): Fragment {
            val fragment = ProductDetailsFragment()
            fragment.arguments = arguments
            return fragment
        }
    }
}
