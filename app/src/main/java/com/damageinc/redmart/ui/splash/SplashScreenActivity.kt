package com.damageinc.redmart.ui.splash

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.damageinc.redmart.ui.productlist.ProductListActivity


class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Handler().postDelayed({
            ProductListActivity.start(this)
            finish()
        }, 1000)
    }
}
