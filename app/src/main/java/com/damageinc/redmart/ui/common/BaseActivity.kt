package com.damageinc.redmart.ui.common

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.damageinc.redmart.common.OnResultListener

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity(), OnResultListener {

    companion object {
        val TOOLBAR_TITLE_ARG = "toolbar_title"
        val TOOLBAR_SUBTITLE_ARG = "toolbar_subtitle"
    }

    override fun onResult(senderTag: String?, result: OnResultListener.Result, args: Bundle?) {
        val title = args?.getString(TOOLBAR_TITLE_ARG, "")
        val subtitle = args?.getString(TOOLBAR_SUBTITLE_ARG, "")
        supportActionBar?.setDisplayShowTitleEnabled(true)
        if (title?.isNotEmpty() == true) {
            supportActionBar?.setTitle(title)
        }
        if (subtitle?.isNotEmpty() == true) {
            supportActionBar?.setSubtitle(subtitle)
        }
    }
}