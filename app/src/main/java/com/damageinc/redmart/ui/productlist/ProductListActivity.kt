package com.damageinc.redmart.ui.productlist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.damageinc.redmart.R
import com.damageinc.redmart.domain.models.Product
import com.damageinc.redmart.domain.viewmodels.ProductListViewModel
import com.damageinc.redmart.ui.common.BaseActivity
import com.damageinc.redmart.ui.productdetails.ProductDetailsActivity
import com.damageinc.redmart.ui.productdetails.ProductDetailsFragment
import kotlinx.android.synthetic.main.activity_item_list.*
import kotlinx.android.synthetic.main.item_list.*


class ProductListActivity : BaseActivity(), SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    lateinit var viewModel: ProductListViewModel
    var productListAdapter: ProductListAdapter? = null

    companion object {

        fun start(parent: Context) {
            parent.startActivity(Intent(parent, ProductListActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_list)

        setSupportActionBar(toolbar)
        toolbar.title = title

        viewModel = ViewModelProviders.of(this).get(ProductListViewModel::class.java)
        if (savedInstanceState == null) {
            showLoading()
            viewModel.getNextPage()
        } else {
            viewModel.products.value?.let { products -> setupRecyclerView(productList, products) }
        }

        viewModel.products.observe(this, Observer<MutableList<Product>> { products ->
            products?.let {
                if (viewModel.isOnlyFirstPageLoaded()) {
                    setupRecyclerView(productList, products)
                } else {
                    updateRecyclerView(products)
                    productList.smoothScrollBy(0, 200)
                }
            }
        })
        viewModel.errorMessage.observe(this, Observer<String> {
            it?.let { Toast.makeText(this, it, Toast.LENGTH_LONG).show() }
        })
    }

    override fun onStart() {
        super.onStart()
        swiperefresh.setOnRefreshListener(this)
    }

    private fun setupRecyclerView(recyclerView: RecyclerView, products: MutableList<Product>) {
        productListAdapter = ProductListAdapter(this, products)
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
        recyclerView.adapter = productListAdapter
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                val visibleItemCount = layoutManager.getChildCount()
                val totalItemCount = layoutManager.getItemCount()
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

                if (!viewModel.isLoading && !viewModel.isLastPageLoaded()) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                        viewModel.getNextPage()
                        showLoading()
                    }
                }
            }
        })
        hideLoading()
    }

    private fun updateRecyclerView(products: List<Product>) {
        hideLoading()
        productListAdapter?.addProducts(products)
    }

    override fun onRefresh() {
        viewModel.resetPage()
        productListAdapter?.products = viewModel.products.value
    }

    override fun onClick(view: View?) {
        val product = view?.tag as Product
        val argBundle = Bundle().apply { putInt(ProductDetailsFragment.ARG_PRODUCT_ID, product.id!!) }
        val sourceImage = view.findViewById<ImageView>(R.id.productImage)
        ProductDetailsActivity.start(this, argBundle, sourceImage)
    }

    fun hideLoading() {
        swiperefresh.setRefreshing(false)
    }

    fun showLoading() {
        swiperefresh.post({ swiperefresh.setRefreshing(true) })
    }
}
