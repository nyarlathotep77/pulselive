package com.damageinc.redmart.infrastructure.network.jobs

import com.damageinc.redmart.domain.models.ErrorResponse

import java.io.Serializable

import retrofit2.Response

interface JobListener<T> : Serializable {
    fun onStart()
    fun onSuccess(response: Response<T>)
    fun onError(error: ErrorResponse)
    fun onFinish()
}
