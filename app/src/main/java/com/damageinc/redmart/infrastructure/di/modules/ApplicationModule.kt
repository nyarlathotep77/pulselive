package com.damageinc.redmart.infrastructure.di.modules

import android.content.Context
import com.birbit.android.jobqueue.JobManager
import com.birbit.android.jobqueue.config.Configuration
import com.damageinc.redmart.RedMartApp
import com.damageinc.redmart.infrastructure.di.Injector
import com.damageinc.redmart.infrastructure.di.qualifiers.ForApplication
import com.damageinc.redmart.infrastructure.network.retrofit.RedMartRestApi

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import org.greenrobot.eventbus.EventBus
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
open class ApplicationModule {

    /**
     * Allow the application context to be injected but require that it be annotated with
     * [@Annotation][ForApplication] to explicitly differentiate it from an activity context.
     */
    @Provides
    @Singleton
    @ForApplication
    open fun provideApplicationContext(): Context {
        return RedMartApp.instance
    }

    @Provides
    @Singleton
    open fun provideEventBus(): EventBus {
        return EventBus.getDefault()
    }

    @Provides
    @Singleton
    open fun provideJobManager(@ForApplication context: Context): JobManager {
        val configuration = Configuration.Builder(context)
                .id(JOB_MANAGER_DEFAULT_DB_NAME)
                .build()
        return JobManager(configuration)
    }

    @Provides
    @Singleton
    open fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl("https://api.redmart.com/")
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    open fun provideRedMartRestApi(retrofit : Retrofit): RedMartRestApi {
        return retrofit.create(RedMartRestApi::class.java)
    }

    companion object {
        protected val JOB_MANAGER_DEFAULT_DB_NAME = "redmart_job_queue"
    }
}
