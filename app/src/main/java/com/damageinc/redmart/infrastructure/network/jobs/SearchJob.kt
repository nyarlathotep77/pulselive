package com.damageinc.redmart.infrastructure.network.jobs

import com.damageinc.redmart.domain.models.network.search.SearchRequest
import com.damageinc.redmart.domain.models.network.search.SearchResponse
import com.damageinc.redmart.infrastructure.di.Injector
import com.damageinc.redmart.infrastructure.network.jobs.BaseRestJob
import com.damageinc.redmart.infrastructure.network.jobs.JobListener
import retrofit2.Call

class SearchJob(request: SearchRequest) : BaseRestJob<SearchRequest, SearchResponse>(request) {

    init {
        Injector.instance.applicationComponent?.inject(this)
    }

    override fun getCall(request: SearchRequest): Call<SearchResponse>? {
        return redMartRestApi.search(request.page, request.pageSize)
    }

}