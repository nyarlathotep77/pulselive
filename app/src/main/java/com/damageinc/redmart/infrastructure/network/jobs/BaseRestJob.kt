package com.damageinc.redmart.infrastructure.network.jobs

import com.birbit.android.jobqueue.Job
import com.birbit.android.jobqueue.Params
import com.birbit.android.jobqueue.RetryConstraint
import com.damageinc.redmart.domain.models.ErrorResponse
import com.damageinc.redmart.domain.models.network.BaseRequest
import com.damageinc.redmart.domain.models.network.BaseResponse
import com.damageinc.redmart.infrastructure.di.Injector
import com.damageinc.redmart.infrastructure.network.retrofit.RedMartRestApi
import org.greenrobot.eventbus.EventBus

import java.io.IOException
import java.net.UnknownServiceException

import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject


abstract class BaseRestJob<R : BaseRequest, S : BaseResponse>(var request: R, val jobParams: Params = Params(PRIORITY).requireNetwork()) : Job(jobParams) {

    protected var call: Call<S>? = null
    protected var throwableError: Throwable? = null
    protected var errorResponse: ErrorResponse? = null
    @Inject
    protected lateinit var redMartRestApi : RedMartRestApi
    @Inject
    protected lateinit var eventBus : EventBus

    override fun onAdded() { }

    @Throws(Throwable::class)
    override fun onRun() {
        val response: Response<S>
        try {
            response = getCall(request)!!.execute()
            if (response.isSuccessful) {
                onResponseSuccess(response)
            } else {
                onResponseError(response)
                /** This could throw a [UnknownServiceException]  */
            }
        } catch (e: Exception) {
            onException(e)
        }
    }

    override fun onCancel(cancelReason: Int, throwable: Throwable?) {
        call?.cancel()
    }

    override fun shouldReRunOnThrowable(throwable: Throwable, runCount: Int, maxRunCount: Int): RetryConstraint? {
        return RetryConstraint.CANCEL
    }

    @Throws(Throwable::class)
    protected fun onException(exception: Exception) {
        onResponseError(exception)
    }

    abstract fun getCall(request: R): Call<S>?

    protected fun onResponseSuccess(response: Response<S>) {
        eventBus.post(response.body())

    }

    protected fun onResponseError(response: Response<S>) {
        errorResponse = ErrorResponse(Throwable(response.message()))
        eventBus.post(errorResponse)
    }

    protected fun onResponseError(throwable: Throwable) {
        errorResponse = ErrorResponse(throwable)
        eventBus.post(errorResponse)
    }

    companion object {
        protected val PRIORITY = 3
    }
}