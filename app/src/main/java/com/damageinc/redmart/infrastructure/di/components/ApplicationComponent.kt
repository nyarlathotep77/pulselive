package com.damageinc.redmart.infrastructure.di.components

import android.content.Context
import com.birbit.android.jobqueue.JobManager
import com.damageinc.redmart.domain.viewmodels.ProductDetailsViewModel
import com.damageinc.redmart.domain.viewmodels.ProductListViewModel
import com.damageinc.redmart.infrastructure.di.modules.ApplicationModule
import com.damageinc.redmart.infrastructure.di.qualifiers.ForApplication
import com.damageinc.redmart.infrastructure.network.jobs.GetProductDetailsJob
import com.damageinc.redmart.infrastructure.network.jobs.SearchJob
import com.damageinc.redmart.ui.common.BaseActivity
import javax.inject.Singleton
import dagger.Component

/**
 * Use this component to inject objects needing [Singleton]-scoped dependencies
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    /**
     * The getters below allow [ApplicationComponent] to expose provided objects to depending Components
     */
    @get:ForApplication
    val context: Context
    val jobManager: JobManager
    fun inject(searchJob: SearchJob)
    fun inject(searchJob: ProductListViewModel)
    fun inject(getProductDetailsJob: GetProductDetailsJob)
    fun inject(productDetailsViewModel: ProductDetailsViewModel)
    fun inject(baseActivity: BaseActivity)
}