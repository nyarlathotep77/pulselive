package com.damageinc.redmart.infrastructure.network.jobs

import com.damageinc.redmart.domain.models.network.productdetails.GetProductDetailsRequest
import com.damageinc.redmart.domain.models.network.productdetails.GetProductDetailsResponse
import com.damageinc.redmart.infrastructure.di.Injector
import com.damageinc.redmart.infrastructure.network.retrofit.RedMartRestApi
import retrofit2.Call

class GetProductDetailsJob(request : GetProductDetailsRequest) : BaseRestJob<GetProductDetailsRequest, GetProductDetailsResponse>(request) {

    init {
        Injector.instance.applicationComponent?.inject(this)
    }

    override fun getCall(request: GetProductDetailsRequest): Call<GetProductDetailsResponse>? {
        return redMartRestApi.getProductDetails(request.productId)
    }
}