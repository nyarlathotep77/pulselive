package com.damageinc.redmart.infrastructure.di.qualifiers

import javax.inject.Qualifier

@Qualifier
annotation class ForApplication
