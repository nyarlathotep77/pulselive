package com.damageinc.redmart.infrastructure.di

import com.damageinc.redmart.infrastructure.di.components.ApplicationComponent

class Injector private constructor() {

    var applicationComponent: ApplicationComponent? = null

    private object SingletonHolder { val INSTANCE = Injector() }

    companion object {
        val instance: Injector by lazy { SingletonHolder.INSTANCE }
    }
}
