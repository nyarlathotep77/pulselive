package com.damageinc.redmart.infrastructure.network.retrofit

import com.damageinc.redmart.domain.models.network.productdetails.GetProductDetailsResponse
import com.damageinc.redmart.domain.models.network.search.SearchResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RedMartRestApi {

    @GET("/v1.6.0/catalog/search")
    fun search(@Query("page") page: Int, @Query("pageSize") pageSize: Int): Call<SearchResponse>

    @GET("/v1.6.0/catalog/products/{productId}")
    fun getProductDetails(@Path("productId") productId: Int): Call<GetProductDetailsResponse>

    companion object {
        val IMAGE_BASE_URL = "http://media.redmart.com/newmedia/200p"
    }
}