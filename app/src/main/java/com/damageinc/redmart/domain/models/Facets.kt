package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Facets : Serializable {

    @Json(name = "categories")
    var categories: List<Category>? = null

}