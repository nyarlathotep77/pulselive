package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Filters_ : Serializable {

    @Json(name = "options")
    var options: List<Option>? = null
    @Json(name = "toggles")
    var toggles: List<Toggle>? = null

}