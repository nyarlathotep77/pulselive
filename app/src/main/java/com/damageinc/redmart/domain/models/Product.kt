package com.damageinc.redmart.domain.models

import android.support.annotation.VisibleForTesting
import com.squareup.moshi.Json
import java.io.Serializable

class Product : Serializable {

    @Json(name = "category_tags")
    var categoryTags: List<String>? = null
    @Json(name = "id")
    var id: Int? = null
    @Json(name = "title")
    var title: String? = null
    @Json(name = "desc")
    var desc: String? = null
    @Json(name = "sku")
    var sku: String? = null
    @Json(name = "categories")
    var categories: List<Int>? = null
    @Json(name = "types")
    var types: List<Int>? = null
    @Json(name = "details")
    var details: Details? = null
    @Json(name = "product_life")
    var productLife: ProductLife? = null
    @Json(name = "filters")
    var filters: Filters? = null
    @Json(name = "img")
    var image: Image? = null
    @Json(name = "images")
    var images: List<Image>? = null
    @Json(name = "measure")
    var measure: Measure? = null
    @Json(name = "pricing")
    var pricing: Pricing? = null
    @Json(name = "warehouse")
    var warehouse: Warehouse? = null
    @Json(name = "attributes")
    var attributes: Attributes? = null
    @Json(name = "description_fields")
    var descriptionFields: DescriptionFields? = null
    @Json(name = "max_days_on_shelf")
    var maxDaysOnShelf: Int? = null
    @Json(name = "promotions")
    var promotions: List<Promotion>? = null
    @Json(name = "inventory")
    var inventory: Inventory? = null

    @VisibleForTesting
    constructor(id : Int?, title: String?) {
        this.id = id
        this.title = title
    }

}