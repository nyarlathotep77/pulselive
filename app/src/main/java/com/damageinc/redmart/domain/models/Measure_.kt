package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Measure_ : Serializable {

    @Json(name = "vol_metric")
    var volMetric: String? = null
    @Json(name = "wt")
    var wt: Double? = null
    @Json(name = "wt_metric")
    var wtMetric: String? = null
    @Json(name = "l")
    var l: Double? = null
    @Json(name = "w")
    var w: Double? = null
    @Json(name = "h")
    var h: Double? = null

}