package com.damageinc.redmart.domain.models.network

import com.squareup.moshi.Json
import java.io.Serializable
import java.net.HttpURLConnection

open class BaseResponse : Serializable {

    @Json(name = "code")
    open var code: Int = HttpURLConnection.HTTP_OK

    fun isSuccessful(): Boolean {
        return code == HttpURLConnection.HTTP_OK
    }
}