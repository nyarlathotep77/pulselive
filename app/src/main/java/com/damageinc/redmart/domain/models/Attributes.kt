package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Attributes: Serializable {

    @Json(name = "dag")
    var dag: List<String>? = null

}