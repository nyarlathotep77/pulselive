package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Inventory : Serializable {

    @Json(name = "atp_status")
    var atpStatus: Int? = null
    @Json(name = "max_sale_qty")
    var maxSaleQty: Int? = null
    @Json(name = "qty_in_carts")
    var qtyInCarts: Int? = null
    @Json(name = "qty_in_stock")
    var qtyInStock: Int? = null
    @Json(name = "stock_status")
    var stockStatus: Int? = null
    @Json(name = "stock_type")
    var stockType: Int? = null
    @Json(name = "atp_lots")
    var atpLots: List<AtpLot>? = null
    @Json(name = "next_available_date")
    var nextAvailableDate: String? = null
    @Json(name = "limited_stock_status")
    var limitedStockStatus: Int? = null

}