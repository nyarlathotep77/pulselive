package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable
import java.math.BigDecimal

class Pricing : Serializable {

    @Json(name = "on_sale")
    var onSale: Double? = null
    @Json(name = "price")
    var price: Double? = null
    @Json(name = "promo_price")
    var promoPrice: Double? = null
    @Json(name = "savings")
    var savings: Double? = null

    companion object {
        fun getFormattedPrice(amount : Double?) : String {
            return "$ $amount"
        }
    }
}