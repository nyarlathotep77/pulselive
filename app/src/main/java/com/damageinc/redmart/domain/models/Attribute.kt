package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Attribute : Serializable {

    @Json(name = "name")
    var name: String? = null
    @Json(name = "content")
    var content: String? = null

}