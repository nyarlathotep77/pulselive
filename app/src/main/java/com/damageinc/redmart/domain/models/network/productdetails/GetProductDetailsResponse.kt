package com.damageinc.redmart.domain.models.network.productdetails

import com.damageinc.redmart.domain.models.Product
import com.damageinc.redmart.domain.models.Status
import com.damageinc.redmart.domain.models.network.BaseResponse
import com.squareup.moshi.Json

class GetProductDetailsResponse : BaseResponse() {

    @Json(name = "total")
    var total: Int? = null
    @Json(name = "product")
    var product: Product? = null
    @Json(name = "page")
    var page: Int? = null
    @Json(name = "status")
    var status: Status? = null
    @Json(name = "page_size")
    var pageSize: Int? = null

}