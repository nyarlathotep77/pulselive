package com.damageinc.redmart.domain.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.birbit.android.jobqueue.JobManager
import com.damageinc.redmart.domain.models.ErrorResponse
import com.damageinc.redmart.domain.models.Product
import com.damageinc.redmart.domain.models.network.search.SearchRequest
import com.damageinc.redmart.domain.models.network.search.SearchResponse
import com.damageinc.redmart.infrastructure.di.Injector
import com.damageinc.redmart.infrastructure.network.jobs.SearchJob
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Response
import javax.inject.Inject

class ProductListViewModel : ViewModel() {

    var products: MutableLiveData<MutableList<Product>> = MutableLiveData()
    var errorMessage: MutableLiveData<String> = MutableLiveData()

    var totalNumberOfProducts : Int? = Int.MIN_VALUE
    var currentPage : Int = -1
    var isLoading : Boolean = false

    @Inject
    lateinit var jobManager : JobManager
    @Inject
    lateinit var eventBus : EventBus

    init {
        Injector.instance.applicationComponent?.inject(this)
        products.value = ArrayList<Product>()
        eventBus.register(this)
    }

    fun resetPage() {
        products.value?.removeAll { true }
        currentPage = -1
        getNextPage()
    }

    fun getNextPage() {
        isLoading = true
        jobManager.addJobInBackground(SearchJob(SearchRequest(++currentPage, DEFAULT_PAGE_SIZE)))
    }

    fun isOnlyFirstPageLoaded() : Boolean {
        return currentPage == 0
    }

    fun isLastPageLoaded() : Boolean {
        return totalNumberOfProducts!! < (currentPage + 1) * DEFAULT_PAGE_SIZE
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onSuccess(response: SearchResponse) {
        isLoading = false;
        totalNumberOfProducts = response.total
        val updatedList = mutableListOf<Product>()
        updatedList.addAll(products.value!!)
        updatedList.addAll(response.products)
        products.value = updatedList
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onError(error: ErrorResponse) {
        isLoading = false
        errorMessage.value = error.message
    }

    override fun onCleared() {
        eventBus.unregister(this)
    }

    companion object {
        val DEFAULT_PAGE_SIZE : Int = 10
    }
}