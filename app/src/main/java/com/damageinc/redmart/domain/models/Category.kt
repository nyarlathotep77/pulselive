package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Category : Serializable {

    @Json(name = "id")
    var id: Int? = null
    @Json(name = "name")
    var name: String? = null
    @Json(name = "uri")
    var uri: String? = null
    @Json(name = "count")
    var count: Int? = null
    @Json(name = "image")
    var image: String? = null

}