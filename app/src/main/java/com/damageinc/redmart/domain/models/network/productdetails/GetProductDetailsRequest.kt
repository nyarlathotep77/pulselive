package com.damageinc.redmart.domain.models.network.productdetails

import com.damageinc.redmart.domain.models.network.BaseRequest

data class GetProductDetailsRequest(val productId : Int) : BaseRequest()