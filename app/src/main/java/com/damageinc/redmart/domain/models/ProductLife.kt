package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class ProductLife : Serializable {

    @Json(name = "time")
    var time: Int? = null
    @Json(name = "metric")
    var metric: String? = null
    @Json(name = "is_minimum")
    var isMinimum: Boolean? = null

}