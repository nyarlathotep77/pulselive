package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Details : Serializable {

    @Json(name = "prod_type")
    var prodType: Int? = null
    @Json(name = "uri")
    var uri: String? = null
    @Json(name = "status")
    var status: Int? = null
    @Json(name = "is_new")
    var isNew: Int? = null
    @Json(name = "storage_class")
    var storageClass: String? = null
    @Json(name = "country_of_origin")
    var countryOfOrigin: String? = null

}