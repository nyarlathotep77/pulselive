package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Warehouse : Serializable {

    @Json(name = "measure")
    var measure: Measure_? = null

}