package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Status : Serializable {

    @Json(name = "message")
    var msg: String? = null
    @Json(name = "code")
    var code: Int? = null

}