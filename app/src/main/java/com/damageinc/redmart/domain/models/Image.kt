package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Image : Serializable {

    @Json(name = "h")
    var h: Int? = null
    @Json(name = "w")
    var w: Int? = null
    @Json(name = "name")
    var name: String? = null
    @Json(name = "position")
    var position: Int? = null

}