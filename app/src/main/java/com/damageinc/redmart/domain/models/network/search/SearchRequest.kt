package com.damageinc.redmart.domain.models.network.search

import com.damageinc.redmart.domain.models.network.BaseRequest
import java.io.Serializable

data class SearchRequest(val page: Int, val pageSize : Int) : BaseRequest(), Serializable