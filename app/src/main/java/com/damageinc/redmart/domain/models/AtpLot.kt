package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class AtpLot : Serializable{

    @Json(name = "from_date")
    var fromDate: String? = null
    @Json(name = "to_date")
    var toDate: String? = null
    @Json(name = "stock_status")
    var stockStatus: Int? = null
    @Json(name = "qty_in_stock")
    var qtyInStock: Int? = null
    @Json(name = "qty_in_carts")
    var qtyInCarts: Int? = null

}