package com.damageinc.redmart.domain.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.birbit.android.jobqueue.JobManager
import com.damageinc.redmart.domain.models.ErrorResponse
import com.damageinc.redmart.domain.models.Product
import com.damageinc.redmart.domain.models.network.productdetails.GetProductDetailsRequest
import com.damageinc.redmart.domain.models.network.productdetails.GetProductDetailsResponse
import com.damageinc.redmart.domain.models.network.search.SearchResponse
import com.damageinc.redmart.infrastructure.di.Injector
import com.damageinc.redmart.infrastructure.network.jobs.GetProductDetailsJob
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Response
import javax.inject.Inject

class ProductDetailsViewModel : ViewModel() {
    val product : MutableLiveData<Product> = MutableLiveData()
    val error : MutableLiveData<String> = MutableLiveData()
    @Inject
    lateinit var jobManager : JobManager
    @Inject
    lateinit var eventBus : EventBus

    init {
        Injector.instance.applicationComponent?.inject(this)
        eventBus.register(this)
    }

    fun getProductDetails(productId: Int) {
        jobManager.addJobInBackground(GetProductDetailsJob(GetProductDetailsRequest(productId)))
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onSuccess(productDetails: GetProductDetailsResponse) {
        product.value = productDetails.product
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onError(errorResponse: ErrorResponse) {
        error.value = errorResponse.message
    }
}