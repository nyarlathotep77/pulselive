package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Type : Serializable {

    @Json(name = "count")
    var count: Int? = null
    @Json(name = "name")
    var name: String? = null
    @Json(name = "uri")
    var uri: String? = null

}