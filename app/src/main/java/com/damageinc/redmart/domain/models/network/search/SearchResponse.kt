package com.damageinc.redmart.domain.models.network.search

import com.damageinc.redmart.domain.models.Facets
import com.damageinc.redmart.domain.models.Filters_
import com.damageinc.redmart.domain.models.Product
import com.damageinc.redmart.domain.models.Status
import com.damageinc.redmart.domain.models.network.BaseResponse
import com.squareup.moshi.Json
import java.io.Serializable

data class SearchResponse(@Json(name = "products")
                          val products: List<Product>,
                          @Json(name = "facets")
                          val facets: Facets? = null,
                          @Json(name = "filters")
                          val filters: Filters_? = null,
                          @Json(name = "on_sale_count")
                          var onSaleCount: Int? = null,
                          @Json(name = "status")
                          var status: Status? = null,
                          @Json(name = "total")
                          var total: Int? = null,
                          @Json(name = "page")
                          var page: Int? = null,
                          @Json(name = "page_size")
                          var pageSize: Int? = null) : BaseResponse(), Serializable