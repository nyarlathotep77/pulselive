package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Measure : Serializable {

    @Json(name = "wt_or_vol")
    var wtOrVol: String? = null
    @Json(name = "size")
    var size: Int? = null

}