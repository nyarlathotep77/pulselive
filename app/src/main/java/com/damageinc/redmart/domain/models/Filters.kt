package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Filters : Serializable {

    @Json(name = "mfr_name")
    var mfrName: String? = null
    @Json(name = "trending_frequency")
    var trendingFrequency: Int? = null
    @Json(name = "is_organic")
    var isOrganic: Int? = null
    @Json(name = "country_of_origin")
    var countryOfOrigin: String? = null
    @Json(name = "vendor_name")
    var vendorName: String? = null
    @Json(name = "brand_name")
    var brandName: String? = null
    @Json(name = "brand_uri")
    var brandUri: String? = null
    @Json(name = "frequency")
    var frequency: Int? = null
    @Json(name = "festive_bbq")
    var festiveBbq: String? = null
    @Json(name = "potluck")
    var potluck: String? = null

}