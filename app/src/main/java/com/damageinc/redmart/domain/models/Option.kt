package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class Option : Serializable {

    @Json(name = "name")
    var name: String? = null
    @Json(name = "uri")
    var uri: String? = null
    @Json(name = "types")
    var types: List<Type>? = null

}