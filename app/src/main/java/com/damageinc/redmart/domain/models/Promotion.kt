package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json

class Promotion {

    @Json(name = "id")
    var id: Int? = null
    @Json(name = "type")
    var type: Int? = null
    @Json(name = "savings_text")
    var savingsText: String? = null
    @Json(name = "promo_label")
    var promoLabel: String? = null
    @Json(name = "current_product_group_id")
    var currentProductGroupId: Int? = null

}