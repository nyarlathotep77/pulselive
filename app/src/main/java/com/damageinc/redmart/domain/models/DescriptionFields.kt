package com.damageinc.redmart.domain.models

import com.squareup.moshi.Json
import java.io.Serializable

class DescriptionFields : Serializable {

    @Json(name = "secondary")
    var secondary: List<Attribute>? = null
    @Json(name = "primary")
    var primary: List<Attribute>? = null

}