package com.damageinc.redmart.domain.models

import com.damageinc.redmart.domain.models.network.BaseResponse
import com.squareup.moshi.Json
import java.net.HttpURLConnection

class ErrorResponse() : BaseResponse() {
    @Json(name = "msg")
    var message: String? = null

    constructor(throwable: Throwable) : this() {
        this.message = throwable.message
    }
}