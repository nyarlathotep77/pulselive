package com.damageinc.redmart.common

import android.os.Bundle

interface OnResultListener {

    enum class Result {
        UNKNOWN,
        OK,
        FAIL,
        GOTO,
        CANCEL
    }

    fun onResult(senderTag: String?, result: Result, args: Bundle?)
}