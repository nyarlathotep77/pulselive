package com.damageinc.redmart

import android.app.Application
import com.damageinc.redmart.infrastructure.di.Injector
import com.damageinc.redmart.infrastructure.di.components.ApplicationComponent
import com.damageinc.redmart.infrastructure.di.components.DaggerApplicationComponent

class RedMartApp : Application() {

    companion object {
        lateinit var instance: RedMartApp
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        Injector.instance.applicationComponent = DaggerApplicationComponent.create()
    }
}